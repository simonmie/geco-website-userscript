// ==UserScript==
// @name geco website userscript
// @namespace Violentmonkey Scripts
// @match https://geco.ethz.ch/*
// @grant none
// @description:en geco website userscript
// @version 1
// @description geco website userscript
// @run-at document-idle
// ==/UserScript==


// implemented so far: user highlight on seating map
// use with url params like this:
// https://geco.ethz.ch/lan/seating?username[]=SKY-TOBI&username[]=ferox


// GLOBAL CONSTANTS

var apiURL = 'https://geco.ethz.ch/api/v2/lan/seats';
var userQueryParam = 'username[]';
var seatQueryParam = 'seatno[]';
var seatHighlightColour = 'cyan';
var seatHighlightAnimation = 'blink 0.5s steps(1, start) 10';
var teamMgrUrl = 'https://geco.ethz.ch/lan/teammanager/';
var seatmapUrl = 'https://geco.ethz.ch/lan/seating';
var userPageUrl = 'https://geco.ethz.ch/user/'


// LIBRARY

// class to cache json from api request
function ApiJson(url)
{
    var json = null;
    var err = false;

    var xmlhttp=new XMLHttpRequest();
    xmlhttp.open("GET",url,false);
    //xmlhttp.overrideMimeType('application/xml');
    xmlhttp.send();
    if(xmlhttp.status == 200)
    {
        json = JSON.parse(xmlhttp.responseText);
    }
    else
    {
        err = true;
    }

    this.arrQuery = function(qName,qVal,rName)
    {
        if(null === json || true === err) return null;
        for(i of json)
        {
            if(i[qName] == qVal) return i[rName];
        }
        return null;
    };
}


// HILIGHT CODE

if(String(window.location).startsWith(seatmapUrl))
{
    var usp = new URLSearchParams(window.location.search);
    if(usp.get(userQueryParam) !== null || usp.get(seatQueryParam) !== null)
    {
        var usersToHighlight = usp.getAll(userQueryParam);
        var seatsToHighlight = usp.getAll(seatQueryParam);
        var j = new ApiJson(apiURL);
        for(u of usersToHighlight)
        {
            var seatno = j.arrQuery('username',u,'seatNumber');
            if(null === seatno) continue;
            var seat = document.getElementById(seatno);
            seat.style.backgroundColor = seatHighlightColour;
            seat.style.animation = seatHighlightAnimation;
        }
        for(s of seatsToHighlight)
        {
            var seatno = s;
            if(null === seatno) continue;
            var seat = document.getElementById(seatno);
            seat.style.backgroundColor = seatHighlightColour;
            seat.style.animation = seatHighlightAnimation;
        }
    }
}


// SEAT NUMBER IN TEAM MANAGER

if(String(window.location).startsWith(teamMgrUrl) && Number(String(window.location).slice(teamMgrUrl.length)))
{
  var links = document.getElementsByTagName('a');
  for(a of links)
  {
    if(a.href.startsWith(userPageUrl) && Number(a.href.slice(userPageUrl.length)))
    {
      var textn = a.nextSibling;
      if(textn && textn.textContent)
      {
        var textc = textn.textContent.trim();
        var textm = textc.match(/\[\d+\]/);
        if(textm && textm[0] == textc)
        {
          var seatnr = parseInt(textc.match(/\d+/), 10);
          var link = document.createElement('a');
          link.innerHTML = textn.textContent.trim();
          link.href=seatmapUrl+'?'+seatQueryParam+'='+String(seatnr);
          textn.replaceWith(link);
          link.insertAdjacentText("beforebegin", " ");
        }
      }
    }
  }
}
